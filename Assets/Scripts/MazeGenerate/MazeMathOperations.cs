public static class MazeMathOperations
{

    public static int CountSquareNeighbours(int x, int z)
    {
        int count = 0;
        //Границы, за пределы которых не должна выходить рекурсия
        if (x <= 0 || x >= Constants.MAZE_WIDTH - 1 || z <= 0 || z >= Constants.MAZE_DEPTH - 1) return 5;
        if (Constants.map[x - 1, z] == 0) count++;
        if (Constants.map[x + 1, z] == 0) count++;
        if (Constants.map[x, z + 1] == 0) count++;
        if (Constants.map[x, z - 1] == 0) count++;
        return count;
    }
    
    public static int CountDiagonalNeighbours(int x, int z)
    {
        int count = 0;
        if (x <= 0 || x >= Constants.MAZE_WIDTH - 1 || z <= 0 || z >= Constants.MAZE_DEPTH - 1) return 5;
        if (Constants.map[x - 1, z - 1] == 0) count++;
        if (Constants.map[x + 1, z + 1] == 0) count++;
        if (Constants.map[x - 1, z + 1] == 0) count++;
        if (Constants.map[x + 1, z - 1] == 0) count++;
        return count;
    }
    
    public static int CountAllNeighbours(int x, int z)
    {
        return MazeMathOperations.CountSquareNeighbours(x,z) + CountDiagonalNeighbours(x,z);
    }
}