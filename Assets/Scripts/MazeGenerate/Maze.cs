﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour
{
    private readonly Sample_Generate _sampleGenerate;

    public Maze()
    {
        _sampleGenerate = new Sample_Generate();
       
    }

    void Start()
    {
        Generate();
    }

    public virtual void Generate()
    {
        _sampleGenerate.Generate();
    }
   
}
