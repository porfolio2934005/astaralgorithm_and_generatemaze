using UnityEngine;

public class Sample_Generate 
{
    public virtual void Generate()
    {
        for (int z = 0; z < Constants.MAZE_DEPTH; z++)
        for (int x = 0; x < Constants.MAZE_WIDTH; x++)
        {
            if(Random.Range(0,100) < 50) Constants.map[x, z] = 0;     //1 = wall  0 = corridor
        }
    }
}