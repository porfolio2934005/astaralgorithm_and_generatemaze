﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recursive : Maze
{
    public override void Generate()
    {
        Generate(5, 5);
    }

    public void Generate(int x, int z)
    {
        if (MazeMathOperations.CountSquareNeighbours(x, z) >= 2) return;
        Constants.map[x, z] = 0;

        Constants.directions.Shuffle();

        Generate(x + Constants.directions[0].x, z + Constants.directions[0].z);
        Generate(x + Constants.directions[1].x, z + Constants.directions[1].z);
        Generate(x + Constants.directions[2].x, z + Constants.directions[2].z);
        Generate(x + Constants.directions[3].x, z + Constants.directions[3].z);
    }

    void DrawMap()
    {
        for (int z = 0; z < Constants.MAZE_DEPTH; z++)
        for (int x = 0; x < Constants.MAZE_WIDTH; x++)
        {
            if (Constants.map[x, z] == 1)
            {
                Vector3 pos = new Vector3(x * Constants.SCALE, 0, z * Constants.SCALE);
                GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
                wall.transform.localScale = new Vector3(Constants.SCALE, Constants.SCALE, Constants.SCALE);
                wall.transform.position = pos;
                wall.transform.parent = this.transform;
            }
        }
    }
    
    void InitialiseMap()
    {
        Constants.map = new byte[Constants.MAZE_WIDTH,Constants.MAZE_DEPTH];
        for (int z = 0; z < Constants.MAZE_DEPTH; z++)
        for (int x = 0; x < Constants.MAZE_WIDTH; x++)
        {
            Constants.map[x, z] = 1;     //1 = wall  0 = corridor
        }
    }

    void Start()
    {
        InitialiseMap();
        Generate();
        DrawMap();
    }
}
