using System.Collections;
using System.Collections.Generic;
using Factory;
using Infrastructure;
using UnityEngine;

public class PathClient 
{
    private readonly AllServices _services;
    private readonly IFactoryPath _pathFactory;
    public PathClient(AllServices services,IFactoryPath pathFactory)
    {
        _services = services;
        _services.RegisterSingle<IAsserts>(new Assert());
        _pathFactory = pathFactory;
    }

    public void Main()
    {

        Debug.Log("Тестирование когда с первым типом фабрики Дейкстра");
        Vector3 vec = new Vector3(1f,1f,1f);
        PathMarker start = _pathFactory.CreateFirstPoint(vec);
        //ClientMethod(new DijkstraFactory(_services));
        
        Debug.Log("Тестирование когда со вторым типом фабрики А-Звёздочка");
        //ClientMethod(new AStarFactory());
    }

    public void ClientMethod(IFactoryPath factory)
    {
        Vector3 vec1 = new Vector3(1f, 1f, 1f);
        Vector3 vec2 = new Vector3(3f, 3f, 3f);
        Vector3 vec3 = new Vector3(5f, 5f, 5f);
        var first = factory.CreateFirstPoint(vec1);
        var intermediate = factory.CreateIntermediatePoint(vec2);
        var finish = factory.CreateFinishPoint(vec3);
    }
    
    
}