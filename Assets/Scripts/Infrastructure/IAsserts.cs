using UnityEngine;

namespace Factory
{
    public interface IAsserts:IService
    {
        GameObject Instantiate(string path);

        GameObject Instantiate(string path, Vector3 at);
    }
}