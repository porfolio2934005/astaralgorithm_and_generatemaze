using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FindPathAStar : MonoBehaviour {

    //TODO - здесь сильно много зависимостей, в будущем будут убраны
    public Maze maze;
    public Material closedMaterial;
        //public Material openMaterial;
    public GameObject start;
    public GameObject end;
    public GameObject pathP;

    PathMarker startNode;
    PathMarker goalNode;
    
    //Эта переменная будет использоваться постоянно для пораждения нового маркера
    PathMarker lastPos;
    bool done = false;
    bool hasStarted = false;

    List<PathMarker> open = new List<PathMarker>();
    List<PathMarker> closed = new List<PathMarker>();

        
    void RemoveAllMarkers() {

        GameObject[] markers = GameObject.FindGameObjectsWithTag("marker");

        foreach (GameObject m in markers) Destroy(m);
    }

    void BeginSearch() {

        done = false;  //Флаг завершения выключен
        //TODO удаление маркеров
        RemoveAllMarkers();  //Удаление маркеров

        List<MapLocation> locations = new List<MapLocation>();

        //Cоздание ячеек для перемещения марекров
        CreateMapTitles(locations);
        locations.Shuffle();
        
        //Инициализация вектора и маркера для стартовой позиции
        Vector3 startLocation = new Vector3(locations[0].x * Constants.SCALE, 0.0f, locations[0].z * Constants.SCALE);
        startNode = new PathMarker(new MapLocation(locations[0].x, locations[0].z),
            0.0f, 0.0f, 0.0f, Instantiate(start, startLocation, Quaternion.identity), null);

        //Инициализация вектора и маркера для финишной позиции
        Vector3 endLocation = new Vector3(locations[1].x * Constants.SCALE, 0.0f, locations[1].z * Constants.SCALE);
        goalNode = new PathMarker(new MapLocation(locations[1].x, locations[1].z),
            0.0f, 0.0f, 0.0f, Instantiate(end, endLocation, Quaternion.identity), null);

        open.Clear();
        closed.Clear();

        open.Add(startNode);
        lastPos = startNode;
    }

    private static void CreateMapTitles(List<MapLocation> locations)
    {
        for (int z = 1; z < Constants.MAZE_DEPTH - 1; ++z)
        {
            for (int x = 1; x < Constants.MAZE_WIDTH - 1; ++x)
            {
                if (Constants.map[x, z] != 1)
                {
                    locations.Add(new MapLocation(x, z));
                }
            }
        }
    }

    void Search(PathMarker thisNode) {

        //Проверка на Null
        if (thisNode == null) return;
        if (thisNode.Equals(goalNode)) {

            done = true;
            // Debug.Log("DONE!");
            return;
        }

        foreach (MapLocation dir in Constants.directions) {
            MapLocation neighbour = dir + thisNode.location;
            //Если врезаеться в стену
            if (Constants.map[neighbour.x, neighbour.z] == 1) continue;

            if (neighbour.x < 1 || neighbour.x >= Constants.MAZE_WIDTH || neighbour.z < 1 || neighbour.z >= Constants.MAZE_DEPTH) 
                continue;
            //Они будут проходить сквозь друг - друга  
            if (IsClosed(neighbour)) continue;

            //Расчёт дистанции
            float g = Vector2.Distance(thisNode.location.ToVector(), neighbour.ToVector()) + thisNode.G;
            float h = Vector2.Distance(neighbour.ToVector(), goalNode.location.ToVector());
            float f = g + h;
            
            //Спавн объекта
            GameObject pathBlock = Instantiate(pathP, new Vector3(neighbour.x * Constants.SCALE, 0.0f, neighbour.z * Constants.SCALE), Quaternion.identity);

            //-----------------------------------------Зона InputService-------------------------------
            //Получение текста
            BaseOutput.GetOutputValue(pathBlock, g, h, f);

            //-----------------------------------------Зона InputService------------------------------- 
            
            
            if (!UpdateMarker(neighbour, g, h, f, thisNode)) {

                open.Add(new PathMarker(neighbour, g, h, f, pathBlock, thisNode));
            }
        }
        //----------------------------------------Зона ответственности сортировщика -------------------------
        open = open.OrderBy(p => p.F).ToList<PathMarker>();
        //----------------------------------------Зона ответственности сортировщика -------------------------
        
        PathMarker pm = (PathMarker)open.ElementAt(0);

        closed.Add(pm);

        open.RemoveAt(0);
        pm.marker.GetComponent<Renderer>().material = closedMaterial;

        lastPos = pm;
    }

    bool UpdateMarker(MapLocation pos, float g, float h, float f, PathMarker prt) {

        foreach (PathMarker p in open) {

            if (p.location.Equals(pos)) {

                p.G = g;
                p.H = h;
                p.F = f;
                p.parent = prt;
                return true;
            }
        }
        return false;
    }

    bool IsClosed(MapLocation marker) {

        foreach (PathMarker p in closed) {

            if (p.location.Equals(marker)) return true;
        }
        return false;
    }

    void GetPath() {

        RemoveAllMarkers();
        PathMarker begin = lastPos;

        while (!startNode.Equals(begin) && begin != null) {

            Instantiate(pathP, new Vector3(begin.location.x * Constants.SCALE, 0.0f, begin.location.z * Constants.SCALE), Quaternion.identity);
            begin = begin.parent;
        }

        Instantiate(pathP, new Vector3(startNode.location.x * Constants.SCALE, 0.0f, startNode.location.z * Constants.SCALE), Quaternion.identity);
    }

    void Update() {

        if (Input.GetKeyDown(KeyCode.P)) {

            BeginSearch();
            hasStarted = true;
        }

        if (hasStarted) {
            if (Input.GetKeyDown(KeyCode.C) && !done) Search(lastPos);
        }

        if (Input.GetKeyDown(KeyCode.M)) {

            GetPath();
        }
    }
}