using Factory;
using UnityEngine;

public class AStarFactory : FactoryPath
{
    private readonly IAsserts _asserts;

    public AStarFactory(IAsserts asserts)
        : base(asserts)
    {
        
    }





    public override PathMarker CreateFirstPoint(Vector3 vec)
    {
        Debug.Log("Создание стартовой точки алгоритма А_Звёздочка");
        return new PathMarker(new MapLocation(0,0), 0.0f, 0.0f, 0.0f, _asserts.Instantiate(Constants.ASTAR_PATH_START_POINT,vec), null);
    }

    public override PathMarker CreateFinishPoint(Vector3 vec)
    {
        Debug.Log("Создание конечной точки алгоритма А_Звёздочка");
        return new PathMarker(new MapLocation(0,0), 0.0f, 0.0f, 0.0f, _asserts.Instantiate(Constants.ASTAR_PATH_END_POINT,vec), null);
    }

    public override PathMarker CreateIntermediatePoint(Vector3 vec)
    {
        Debug.Log("Создание промежуточной точки алгоритма А_Звёздочка");
        return new PathMarker(new MapLocation(0,0), 0.0f, 0.0f, 0.0f, _asserts.Instantiate(Constants.ASTAR_PATH_INTERMEDIATE_POINT,vec), null);
    }
}