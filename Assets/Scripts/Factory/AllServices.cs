namespace Factory
{
    public class AllServices
    {
        private static AllServices _instance;
        public static AllServices Container => _instance ?? (_instance = new AllServices());

        public void RegisterSingle<TService>(TService implementation) where TService : IService
        {
            //Получение реализации
            Implementation<TService>.ServiceInstance = implementation;
        }

        //Возвращает TService по запросу этого сервиса
        public TService Single<TService>() where TService : IService
        {
            return Implementation<TService>.ServiceInstance;
        }

        //ServiceInstance - это класс, в который будет втыкаться реализация
        private static class Implementation<TService>
        {
            public static TService ServiceInstance;
        }
    }
}