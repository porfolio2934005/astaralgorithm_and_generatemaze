using Factory;
using UnityEngine;

public class DijkstraFactory : FactoryPath
{
    private readonly IAsserts _asserts;
    public DijkstraFactory(IAsserts asserts)
        : base(asserts)                  
    {                                    
                                     
    }                                    
    public override PathMarker CreateFirstPoint(Vector3 at)
    {
        Debug.Log("Создание стартовой точки алгоритма Дейкстра");
        return new PathMarker(new MapLocation(0,0), 0.0f, 0.0f, 0.0f, _asserts.Instantiate(Constants.DIJKSTRA_PATH_START_POINT,at), null);
    }

    public override PathMarker CreateFinishPoint(Vector3 at)
    {
        Debug.Log("Создание конечной точки алгоритма Дейкстра");
        return new PathMarker(new MapLocation(0,0), 0.0f, 0.0f, 0.0f, _asserts.Instantiate(Constants.DIJKSTRA_PATH_END_POINT,at), null);
    }  

    public override PathMarker CreateIntermediatePoint(Vector3 at)
    {
        Debug.Log("Создание стартовой точки алгоритма Дейкстра");
        return new PathMarker(new MapLocation(0,0), 0.0f, 0.0f, 0.0f, _asserts.Instantiate(Constants.DIJKSTRA_PATH_INTERMEDIATE_POINT,at), null);
    }
}