using UnityEngine;

namespace Factory
{
    abstract public class FactoryPath : IFactoryPath
    {
        private readonly IAsserts _asserts;

        public FactoryPath(IAsserts asserts)
        {
            _asserts = asserts;
        }

        virtual public PathMarker CreateFirstPoint(Vector3 vec)
        {
            //return _asserts.Instantiate()
            throw new System.NotImplementedException();
        }

        virtual public PathMarker CreateFinishPoint(Vector3 vec)
        {
            throw new System.NotImplementedException();
        }

        virtual public PathMarker CreateIntermediatePoint(Vector3 vec)
        {
            throw new System.NotImplementedException();
        }
    }
}