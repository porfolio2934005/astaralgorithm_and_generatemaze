using UnityEngine;

namespace Factory
{
    public interface IFactoryPath
    {
        PathMarker CreateFirstPoint(Vector3 vec);

        PathMarker CreateFinishPoint(Vector3 vec);

        PathMarker CreateIntermediatePoint(Vector3 vec);
    }
}