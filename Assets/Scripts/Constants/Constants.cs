using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
    public const string DIJKSTRA_PATH_END_POINT = "Assets/Models/Prefabs/Dijkstra/End";
    public const string DIJKSTRA_PATH_INTERMEDIATE_POINT = "Assets/Models/Prefabs/Dijkstra/Path";
    public const string DIJKSTRA_PATH_START_POINT = "Assets/Models/Prefabs/Dijkstra/Start";
    
    public const string ASTAR_PATH_END_POINT = "Assets/Models/Prefabs/AStar/End";
    public const string ASTAR_PATH_INTERMEDIATE_POINT = "Assets/Models/Prefabs/AStar/Path";
    public const string ASTAR_PATH_START_POINT = "Assets/Models/Prefabs/AStar/Start";

    public const float FLOAT_ZERO_VALUE = 0.00f;
    public const int INT_ZERO_VALUE = 0;


    #region Maze

        public static byte[,] map;
    
        public static List<MapLocation> directions = new List<MapLocation>() {
        new MapLocation(1,0),
        new MapLocation(0,1),
        new MapLocation(-1,0),
        new MapLocation(0,-1) };
        
        
        public static  int MAZE_WIDTH = 30; //x length
        public static  int MAZE_DEPTH = 30; //z length
        public static int SCALE = 5;


        public static int START_X_POINT = 5;
        public static int START_Y_POINT = 5;

        #endregion

}
