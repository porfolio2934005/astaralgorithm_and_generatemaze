using UnityEngine;

static internal class BaseOutput
{
    public static void GetOutputValue(GameObject pathBlock, float g, float h, float f)
    {
        TextMesh[] values = pathBlock.GetComponentsInChildren<TextMesh>();

        //Запись значений в текст
        values[0].text = "G: " + g.ToString("0.00");
        values[1].text = "H: " + h.ToString("0.00");
        values[2].text = "F: " + f.ToString("0.00");
    }
}